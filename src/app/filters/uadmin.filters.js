/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UAdmin.filters', [])
        .filter('removeVendorPrefix',removeVendorPrefix);


    function removeVendorPrefix(GroupContactsService) {
        return function (grpname) {
            return grpname.replace(GroupContactsService.getVendorPrefix(), "");
        };
    }

})();
