/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UAdmin.auth', []).
        controller('LoginController',
        ['$scope', '$rootScope', '$location', 'AuthenticationService', 'uMsg',
            function ($scope, $rootScope, $location, AuthenticationService, uMsg) {
                // reset login status
                AuthenticationService.ClearCredentials();

                $scope.login = function () {
                    $scope.dataLoading = true;
                    AuthenticationService.Login($scope.username, $scope.password, function (response) {
                        if (response.status == "200" && response.data != "") {
                            AuthenticationService.SetCredentials($scope.username, $scope.password, response.data.user_fullname, response.data.user_id, response.data.user_sender, response.data.user_credit);
                            uMsg.setSender($rootScope.globals.currentUser.senderid);
                            uMsg.setRoute("4");
                            $location.path('/');
                        } else {
                            $scope.error = response.message;
                            $scope.dataLoading = false;
                        }
                    });
                };
            }]).config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('auth', {
                url: '/login',
                views: {
                    'auth': {
                        controller: 'LoginController',
                        templateUrl: 'app/auth/auth.html'
                    }
                },
                title: 'Login to UAdmin',
            });
    };


})();
