/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('UAdmin.pages', [
    'ui.router',
      'UAdmin.pages.commonSMS',
      'UAdmin.pages.groups',
      'UAdmin.pages.reports',
      'UAdmin.pages.help'

  ])
      .config(routeConfig).run(['$rootScope', '$location', '$cookieStore', '$http', 'uMsg', 'UserService',
          function ($rootScope, $location, $cookieStore, $http, uMsg, UserService) {
              // keep user logged in after page refresh
              $rootScope.globals = $cookieStore.get('globals') || {};
              if ($rootScope.globals.currentUser) {
                  $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
                  uMsg.setSender($rootScope.globals.currentUser.senderid);
                  uMsg.setRoute("4");
              }

              $rootScope.$on('$locationChangeStart', function (event, next, current) {
                  // redirect to login page if not logged in
                  if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                      $location.path('/login');
                  } else if($location.path() !== '/login'){
                      UserService.GetBalance($rootScope.globals.currentUser.userid, function(response){
                          if(response.data!=""){
                              $rootScope.globals.currentUser.balance = response.data;
                          } else {
                              $rootScope.globals.currentUser.balance = 0;
                          }
                      });
                  }
              });

          }]);

    /** @ngInject */
    function routeConfig($urlRouterProvider, baSidebarServiceProvider) {
        $urlRouterProvider.otherwise('/sendResults');
    }


})();
