/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UAdmin.pages.reports', [])
        .config(routeConfig).controller('ReportCtrl', ReportCtrl);

    /** @ngInject */
    function ReportCtrl($scope,$http, uMsg) {

        $scope.reportArr = [];
        $scope.filtered = [];

        console.info(uMsg.getSenderId());
        $http.get("rest/getreports?senderid=" + uMsg.getSenderId().toLowerCase()).then(function (response) {
            $scope.reportArr = response.data;
        });


    };
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('reports', {
                url: '/reports',
                controller: ReportCtrl,
                templateUrl: 'app/pages/reports/reports.html',
                title: 'Delivery Reports',
                sidebarMeta: {
                    icon: 'ion-arrow-graph-up-right',
                    order: 2,
                },
            });
    }

})();
