/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UAdmin.pages.help', [])
        .config(routeConfig).controller('HelpCtrl', HelpCtrl);

    /** @ngInject */
    function HelpCtrl($scope) {


    };
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('help', {
                url: '/help',
                controller: HelpCtrl,
                templateUrl: 'app/pages/help/help.html',
                title: 'Help',
                sidebarMeta: {
                    icon: 'ion-help-circled',
                    order: 3,
                },
            });
    }

})();
