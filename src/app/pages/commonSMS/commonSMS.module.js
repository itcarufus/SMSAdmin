/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UAdmin.pages.commonSMS', [])
        .config(routeConfig).controller('CommonSMSCtrl', CommonSMSCtrl);

    /** @ngInject */
    function CommonSMSCtrl($scope,  uMsg, toastr, GroupContactsService) {

        $scope.vm = {};
        $scope.vm.transactionalForm = {};
        $scope.vm.transactionalInfo = {};
        $scope.vm.transactionalInfo.mobilenos = "";
        $scope.vm.transactionalInfo.message = "";
        $scope.balance4 = 0;

        if(typeof GroupContactsService.getGroupSMSList() != "undefined"){
            $scope.vm.transactionalInfo.mobilenos = GroupContactsService.getGroupSMSList();
            GroupContactsService.setGroupSMSList(undefined);
        }



        //Send Button Click
        $scope.sendTransactionalMessage = function (isValid) {
            if(isValid){
                uMsg.send($scope.vm.transactionalInfo.mobilenos, $scope.vm.transactionalInfo.message, function (err, response) {
                    if(response.type == "error" || response.msgType == "error" || response.msg_type == "error"){
                        toastr.error(response.message, 'Error');
                    } else {
                        toastr.success("Message sent.", 'Success');
                    }
                });
            }
        };

      
    };


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('commonSMS', {
                url: '/commonSMS',
                controller: CommonSMSCtrl,
                templateUrl: 'app/pages/commonSMS/commonSMS.html',
                title: 'Send Common SMS',
                sidebarMeta: {
                    icon: 'ion-paper-airplane',
                    order: 1,
                },
            });
    }

})();
