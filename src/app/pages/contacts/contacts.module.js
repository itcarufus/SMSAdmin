/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UAdmin.pages.contacts', ['ngSanitize'])
        .controller('ContactsCtrl', ContactsCtrl);

    /** @ngInject */
    function ContactsCtrl($rootScope, $scope, uMsg, toastr, GroupContactsService, editableOptions, editableThemes) {

        $scope.groupInfo = {};
        $scope.contactList = [];

        $scope.cm = {};
        $scope.cm.contactCreationForm = {};
        $scope.cm.newCName = "";
        $scope.cm.newCNumber = "";
        $scope.cm.newCResult = "";

        $scope.cm.header = $rootScope.globals.currentUser.fullname;
        $scope.cm.footer = "";

        $scope.msgQueue = {
            sending : false,
            currentMsg : 0
        };



        $scope.groupInfo = GroupContactsService.getManageGroupInfo();

        updateContactList(uMsg, $scope);

        $scope.propertyFilter = function (contact) {
            return contact.hasOwnProperty("contactid");
        }
        $scope.addNewContact = function (isValid) {
            if (isValid) {

                uMsg.createContact($scope.cm.newCName, $scope.cm.newCNumber, window.encodeURIComponent($scope.cm.newCResult), $scope.groupInfo.id, function (err, response) {
                    if (response.msgType == "error" || response.msg_type == "error") {
                        toastr.error(response.msg, 'Error');
                    } else {
                        updateContactList(uMsg, $scope);
                        toastr.success(response.msg, 'Success');
                    }
                });
                this.cm.newCName = "";
                this.cm.newCNumber = "";
            }
        }
        $scope.deleteContact = function (index) {
            uMsg.removeContact($scope.contactList[index].contactid, function (err, response) {
                if (response.msg_type == "error" || response.msg_type == "error") {
                    toastr.error(response.msg, 'Error');
                } else {
                    $scope.contactList.splice(index, 1);
                    toastr.success(response.msg, 'Success');
                }

            });
        }
        $scope.saveContact = function (index) {
                var rowData = $scope.contactList[index];
                uMsg.createContact(rowData.name, rowData.number,  window.encodeURIComponent(rowData.marks) , $scope.groupInfo.id, function (err, response) {
                    if (response.msgType == "error" || response.msg_type == "error") {
                        toastr.error(response.msg, 'Error');
                    } else {
                        updateContactList(uMsg, $scope);
                        toastr.success(response.msg, 'Success');
                    }
                });
        }
        $scope.sendResults = function (){

            $scope.msgQueue.currentMsg = 0;
            $scope.msgQueue.sending = true;
            messageQueue($scope.msgQueue.currentMsg);
        };
        $scope.sendSingleMarks = function(index){
            var stud = $scope.contactList[index];
            var message = $scope.cm.header + "\n" + stud.marks + "\n" +  $scope.cm.footer;
            uMsg.send(stud.number, message, function (err, response) {
                if(response.type == "error" || response.msgType == "error" || response.msg_type == "error"){
                    toastr.error(response.message, 'Error');
                } else {
                    toastr.success("Message sent.", 'Success');
                }
            });
        }
        function messageQueue(index){
            var stud = $scope.contactList[index];
            var message = $scope.cm.header + "\n" + stud.marks + "\n" +  $scope.cm.footer;
            uMsg.send(stud.number, message, function (err, response) {
                if(response.type == "error" || response.msgType == "error" || response.msg_type == "error"){
                    toastr.error(response.message, 'Error');
                    $scope.msgQueue.sending = false;
                    if(response.message.toLowerCase().indexOf("no balance") >= 0){
                        return;
                    }
                }
                $scope.msgQueue.currentMsg = index + 1;
                if($scope.msgQueue.currentMsg < $scope.contactList.length){
                    messageQueue($scope.msgQueue.currentMsg);
                } else {
                    $scope.msgQueue.sending = false;
                    toastr.success("Message(s) sent.", 'Success');
                }
            });
        }
        editableOptions.theme = 'bs3';
        editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary btn-with-icon"><i class="ion-checkmark-round"></i></button>';
        editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-default btn-with-icon"><i class="ion-close-round"></i></button>';
    };
    function updateContactList(uMsg, $scope) {

        uMsg.getContactList($scope.groupInfo.id, function (err, data) {
            var msgContactList = data.contacts;
            $scope.contactList = [];
            for (var ct in msgContactList) {
                var marksVal = "";
                if (msgContactList[ct].more_fields.length > 0) {
                    var marksFieldID = msgContactList[ct].more_fields[0].$id;
                    marksVal = msgContactList[ct][marksFieldID];
                }

                var elem = document.createElement('textarea');
                elem.innerHTML = marksVal;
                marksVal = elem.value.replace(new RegExp("<br>", 'g'),"\n");
                var cname = msgContactList[ct]["name"];
                var cnumber = msgContactList[ct]["number"];
                $scope.contactList.push({contactid: ct, name : cname, number : cnumber, marks : marksVal});
            }

        });
    }

})();
