/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UAdmin.pages.groups', ['UAdmin.pages.contacts'])
        .config(routeConfig).controller('GroupsCtrl', GroupsCtrl);

    /** @ngInject */
    function GroupsCtrl($scope, uMsg, toastr, $location, $uibModal, filterFilter, GroupContactsService) {

        $scope.groupArr = [];

        var gm = {};
        gm.groupCreationForm = {};
        gm.newGrpName = "";

        //Get global groups list
        uMsg.groupList(function (err, response) {
            //Filter groups for this user
            if (typeof response.msgType == "undefined") {
                var userGrps = filterFilter(response,GroupContactsService.getVendorPrefix());
                $scope.groupArr = userGrps;
            }
        });


        $scope.manageContacts = function ($index) {
            GroupContactsService.setManageGroupInfo($scope.groupArr[$index]);
            $uibModal.open({
                animation: true,
                controller: 'ContactsCtrl',
                templateUrl: 'app/pages/contacts/contacts.html',
                size: 'lg',
                backdrop: 'static',
                keyboard : false
            });
        };

        $scope.addNewGroup = function (isValid) {
            if (isValid) {
                var grpName = GroupContactsService.getVendorPrefix() + this.gm.newGrpName;
                uMsg.createGroup(grpName, function (err, response) {
                    if (response.msgType == "error") {
                        toastr.error(response.msg, 'Error');
                    } else {
                        var newGrp = {
                            count: 0,
                            id: response.grpId,
                            name: response.grpName
                        };
                        $scope.groupArr.push(newGrp);
                        toastr.success(response.msg.replace(GroupContactsService.getVendorPrefix(), ""), 'Success');
                    }
                });
                this.gm.newGrpName = "";
            }
        };


        $scope.deleteGroup = function (index) {
            uMsg.removeGroup($scope.groupArr[index].id, function (err, response) {
                if (response.msgType == "error") {
                    toastr.error(response.msg, 'Error');
                } else {
                    $scope.groupArr.splice(index, 1);
                    toastr.success(response.msg, 'Success');
                }

            });
        }

        $scope.sendGroupSMS = function (index) {
            uMsg.getContactList($scope.groupArr[index].id,function(err,data){
                var contacts = data.contacts;
                if(data.msgType!="error" && Object.keys(contacts).length > 0){
                    var mobilearr = []
                    for(var ct in contacts){
                        mobilearr.push(contacts[ct].number);
                    }
                    GroupContactsService.setGroupSMSList(mobilearr.join());
                    $location.path('/commonSMS');
                } else {
                    toastr.error("No contacts", 'Info');
                }

            });

        }


    };






    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('groups', {
                url: '/sendResults',
                controller: GroupsCtrl,
                templateUrl: 'app/pages/groups/groups.html',
                title: 'Send Results',
                sidebarMeta: {
                    icon: 'ion-university',
                    order: 0,
                },
            });
    }

})();
