/**
 * @author v.lugovsky
 * created on 17.12.2015
 */
(function () {
    'use strict';

    angular.module('UAdmin.theme')
        .filter('helpImg', helpImages);

    /** @ngInject */
    function helpImages(layoutPaths) {
        return function(input, ext) {
            ext = ext || 'png';
            return layoutPaths.images.root + '/app/' + input + '.' + ext;
        };
    }

})();
