/**
 * @author a.demeshko
 * created on 12/21/15
 */
(function () {
    'use strict';

    angular.module('UAdmin.theme')
        .service('uMsg', uMsg);

    /** @ngInject */
    function uMsg($http, $rootScope) {

        var authKey = "114370AghLwsC3U5749b2c9";
        var senderId = "";
        var route = "";


        this.send = function (mobileNos, message, callback) {

            if($rootScope.globals.currentUser.balance <= 0){
                callback(err, {msgtype:"error", message : "No Balance. Please recharge."});
                return;
            }

            callback = modifyCallbackIfNull(callback);

            mobileNos = validateMobileNos(mobileNos);

            message = validateMessage(message);

            var isUnicode = isUnicodeString(message);

            message = window.encodeURIComponent(message);

            var postData = "response=json&authkey=" + authKey + "&sender=" + senderId + "&mobiles=" + mobileNos + "&message=" + message + "&route=" + route;

            postData = {
                response : "json",
                authkey : authKey,
                sender : senderId,
                mobiles : mobileNos,
                message : message,
                route : route
            }
            if (isUnicode) {
                //postData += "&unicode=1";
                postData.unicode = 1;
            }

            var options = {
                path: 'rest/sendsms',
                method: 'POST'
            };

            makeHttpRequest($http, options, postData, function (err, data) {
                callback(err, data);
            });


        };

        this.groupList = function (callback) {

            callback = modifyCallbackIfNull(callback);

            var options = {
                path: 'rest/listgroup?authkey=' + authKey,
                method: 'GET'
            };

            makeHttpRequest($http, options, {}, function (err, data) {
                callback(err, data);
            });


        };
        this.createGroup = function (grpname, callback) {

            callback = modifyCallbackIfNull(callback);

            var options = {
                path: 'rest/addgroup?authkey=' + authKey + '&group_name=' + grpname,
                method: 'GET'
            };

            makeHttpRequest($http, options, {}, function (err, data) {
                callback(err, data);
            });
        };
        this.removeGroup = function (grpid, callback) {
            callback = modifyCallbackIfNull(callback);
            var options = {
                path: 'rest/deletegroup?authkey=' + authKey + '&group_id=' + grpid,
                method: 'GET'
            };

            makeHttpRequest($http, options, {}, function (err, data) {
                callback(err, data);
            });
        };
        this.createContact = function (name, number, marks , grpid, callback) {

            callback = modifyCallbackIfNull(callback);

            var options = {
                path: 'rest/addcontact?authkey=' + authKey + '&name=' +name+ '&mob_no=' +number+ '&group=' + grpid + '&marks='+ marks,
                method: 'GET'
            };

            makeHttpRequest($http, options, {}, function (err, data) {
                callback(err, data);
            });
        };
        this.removeContact = function (contctid, callback) {
            callback = modifyCallbackIfNull(callback);
            var options = {
                path: 'rest/deletecontact?authkey=' + authKey + '&contact_id=' + contctid,
                method: 'GET'
            };

            makeHttpRequest($http, options, {}, function (err, data) {
                callback(err, data);
            });
        };
        this.getContactList = function (grpid, callback) {
            callback = modifyCallbackIfNull(callback);
            var options = {
                //path: 'api/list_contact.php?authkey=' + authKey + '&group=' + grpid,
                path: 'rest/listcontact?authkey=' + authKey + '&group=' + grpid,
                method: 'GET'
            };

            makeHttpRequest($http, options, {}, function (err, data) {
                callback(err, data);
            });
        };

        this.setSender = function(pSenderId){
            senderId = pSenderId;
            if (senderId == null || senderId == "") {
                throw new Error("MSG91 Sender Id is not provided.");
            }
        };
        this.setRoute = function(pRoute){
            route = pRoute;
            if (route == null || route == "") {
                throw new Error("MSG91 router Id is not provided.");
            }
        };

        this.getSenderId = function(){
            return senderId;
        };


        return this;

    }

    function validateMobileNos(mobileNos) {

        if (mobileNos == null || mobileNos == "") {
            throw new Error("MSG91 : Mobile No is not provided.");
        }

        if (mobileNos instanceof Array) {
            mobileNos = mobileNos.join(",");
        }

        return mobileNos
    }

    function validateMessage(message) {

        if (message == null || message == "") {
            throw new Error("MSG91 : message is not provided.");
        }

        return message;
    }

    function modifyCallbackIfNull(callback) {
        return callback || function () {
            };
    }

    function isUnicodeString(str) {
        for (var i = 0, n = str.length; i < n; i++) {
            if (str.charCodeAt(i) > 255) {
                return true;
            }
        }

        return false;
    }

    function makeHttpRequest($http, options, postData, callback) {


        $http({
            url: options.path,
            method: options.method,
            data: typeof postData == 'undefined' ? {} : postData,
            headers: typeof options.headers == 'undefined' ? {} : options.headers,
        }).success(function (data, status, headers, config) {
            console.log(data);
            callback(status, data);
        });


        /*var data = "";
         var req = http.request(options, function (res) {
         res.setEncoding('utf8');
         res.on('data', function (chunk) {
         data += chunk;
         });
         res.on('end', function () {
         callback(null, data);
         })
         });

         req.on('error', function (e) {
         callback(e);
         });

         if (postData != null) {
         req.write(postData);
         }

         req.end();*/

    }

})();