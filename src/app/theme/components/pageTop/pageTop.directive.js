/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UAdmin.theme.components')
        .directive('pageTop', pageTop);

    /** @ngInject */
    function pageTop($rootScope,$location, AuthenticationService, UserService) {
        return {
            restrict: 'E',
            templateUrl: 'app/theme/components/pageTop/pageTop.html',
            link: function (scope, el) {
                scope.logout = function () {
                    AuthenticationService.ClearCredentials();
                    $location.path('/login');
                };
                scope.refreshBalance = function () {
                    UserService.GetBalance($rootScope.globals.currentUser.userid, function (response) {
                        if (response.data != "") {
                            $rootScope.globals.currentUser.balance = response.data;
                        } else {
                            $rootScope.globals.currentUser.balance = 0;
                        }
                    });
                };
            }
        };
    }

})();