'use strict';

angular.module('UAdmin', [
    'ngAnimate',
    'ui.bootstrap',
    'ui.sortable',
    'ui.router',
    'ngTouch',
    'angular-confirm',
    'ngCookies',
    'toastr',
    'smart-table',
    "xeditable",
    'ui.slimscroll',

    'UAdmin.theme',
    'UAdmin.pages',
    'UAdmin.auth',
    'UAdmin.services',
    'UAdmin.filters'

]);