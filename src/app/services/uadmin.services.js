/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UAdmin.services',[]).
        factory('UserService', UserService).
        factory('ResultsService',ResultsService)
        .factory('GroupContactsService',GroupContactsService);

            function ResultsService($http) {
                var service = {};
                service.InsertStudentResult = function (grpID, studID ,resultText, callback) {

                    $http.post("rest/setresults", {
                        "grp_id": grpID,
                        "stud_id": studID,
                        "result": resultText
                    }).then(function (response) {
                        callback(response);
                    });

                };
                service.UpdateStudentResult = function (grpID, studID ,resultText, callback) {

                    $http.put("rest/setresults", {
                        "grp_id": grpID,
                        "stud_id": studID,
                        "result": resultText
                    }).then(function (response) {
                        callback(response);
                    });

                };

                service.GetStudentResult = function (grpID, callback) {

                    $http.get("rest/getresults?grp_id=" + grpID).then(function (response) {
                        callback(response);
                    });

                };


                return service;
            }
    function UserService($http) {
        var service = {};
        service.GetBalance = function(id, callback){
            $http.get("rest/getbalance?userid=" + id).then(function (response) {
                callback(response);
            });
        };
        return service;
    }

    function GroupContactsService($rootScope){
        var curGroupSMSArr = undefined;
        var manageGroupInfo = {};
        var vendorPrefix = '_G' + $rootScope.globals.currentUser.userid + "_";
        this.setGroupSMSList = function(arr){
            curGroupSMSArr = arr;
        };
        this.getGroupSMSList = function(){
            return curGroupSMSArr;
        };

        this.getVendorPrefix = function(){
            return vendorPrefix;
        };

        this.setManageGroupInfo = function(grpInfo){
            manageGroupInfo = grpInfo;
        };
        this.getManageGroupInfo  = function(){
            return manageGroupInfo;
        };
        return this;
    }


})();